module git.gbase.pw/tw/interceptor

go 1.17

require (
	git.gbase.pw/tw/rtcp v0.0.0-20220610223922-5b69f384dab5
	github.com/pion/logging v0.2.2
	github.com/pion/rtp v1.7.13
	github.com/stretchr/testify v1.7.2
)

require (
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/pion/randutil v0.1.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
